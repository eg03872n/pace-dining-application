package com.pace.bb.dining;

import net.rim.device.api.ui.Field;
import net.rim.device.api.ui.FieldChangeListener;
import net.rim.device.api.ui.MenuItem;
import net.rim.device.api.ui.UiApplication;
import net.rim.device.api.ui.component.ButtonField;
import net.rim.device.api.ui.component.Dialog;
import net.rim.device.api.ui.component.LabelField;
import net.rim.device.api.ui.component.SeparatorField;
import net.rim.device.api.ui.container.HorizontalFieldManager;
import net.rim.device.api.ui.container.MainScreen;
import net.rim.device.api.ui.container.VerticalFieldManager;

public class ItemScreen extends MainScreen implements FieldChangeListener {

	private VerticalFieldManager vManager;
	private HorizontalFieldManager hManager;
	private ButtonField buttonAddToOrder;
	private LabelField labelName;
	private LabelField labelCalories;
	private LabelField labelPrice;
	private PaceMenuItem _item;
	private Order _order;
	private SeparatorField s1;
	private SeparatorField s2;
	private SeparatorField s3;

	/**
	 * 
	 */
	public ItemScreen(PaceMenuItem item, Order order) {
		super(MainScreen.VERTICAL_SCROLL | MainScreen.VERTICAL_SCROLLBAR);
		// TODO Auto-generated constructor stub
		_item = item;
		labelName = new LabelField(item.getName());
		labelPrice = new LabelField(item.getPrice());
		labelCalories = new LabelField(item.getCalories());
		_order = order;

		LabelField lName = new LabelField("Name");
		LabelField lCalories = new LabelField("Calories");
		LabelField lPrice = new LabelField("Price");

		this.add(lName);
		this.add(labelName);
		this.add(s1);

		this.add(lCalories);
		this.add(labelCalories);
		this.add(s2);

		this.add(lPrice);
		this.add(labelPrice);
		this.add(s3);

		buttonAddToOrder = new ButtonField("Add item to order",
				ButtonField.CONSUME_CLICK);
		buttonAddToOrder.setChangeListener(this);

		this.add(buttonAddToOrder);

		addMenuItem(new MenuItem("About", 10, 20) {
			public void run() {
				AboutPopupScreen as = new AboutPopupScreen();

				UiApplication.getUiApplication().pushScreen(as);
			}
		});
	}

	public void fieldChanged(Field field, int context) {
		// TODO Auto-generated method stub
		if (field == this.buttonAddToOrder) {
			try {
				int total = _order.addItemToOrder(_item);
				Order order = _order;
				Dialog.alert("This item has been added to your order.");
				String totalMessage = "Your current total is $"
						+ Integer.toString(total) + ".00";
				Dialog.alert(totalMessage);
				OrderScreen os = new OrderScreen(order);
				// UiApplication.getUiApplication().popScreen(getScreen());
				UiApplication.getUiApplication().pushScreen(os);
			} catch (Exception e) {
				Dialog.alert(e.toString());
			}
		} else {
			Dialog.alert("This doesn't work");
		}
	}
}
