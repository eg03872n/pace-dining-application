package com.pace.bb.dining;

import java.util.Vector;

import net.rim.device.api.system.Display;
import net.rim.device.api.ui.Color;
import net.rim.device.api.ui.Field;
import net.rim.device.api.ui.Manager;
import net.rim.device.api.ui.MenuItem;
import net.rim.device.api.ui.UiApplication;
import net.rim.device.api.ui.XYRect;
import net.rim.device.api.ui.component.Dialog;
import net.rim.device.api.ui.component.LabelField;
import net.rim.device.api.ui.component.SeparatorField;
import net.rim.device.api.ui.component.table.DataTemplate;
import net.rim.device.api.ui.component.table.TableController;
import net.rim.device.api.ui.component.table.TableModelAdapter;
import net.rim.device.api.ui.component.table.TableView;
import net.rim.device.api.ui.component.table.TemplateColumnProperties;
import net.rim.device.api.ui.component.table.TemplateRowProperties;
import net.rim.device.api.ui.container.MainScreen;
import net.rim.device.api.ui.decor.BackgroundFactory;

/**
 * A screen demonstrating the use of the Table and List API to display data held
 * in a non-table data structure in table format.
 */
public final class TableAdapterScreen extends MainScreen {
	private MenuTableModelAdapter _tableModel;
	private Vector _items;
	private static final int NUM_ROWS = 1;
	private static final int ROW_HEIGHT = 50;
	private static final int NUM_COLUMNS = 3;

	public TableAdapterScreen(DemoStringTokenizer itemData, 	Order order) {
		super(Manager.NO_VERTICAL_SCROLL);
		setTitle("Pace Menu");

		add(new LabelField("Menu Items", LabelField.FIELD_HCENTER));
		add(new SeparatorField());

		_items = new Vector();

		_tableModel = new MenuTableModelAdapter();

		// Add data to adapter
		while (itemData.hasMoreTokens()) {
			String itemName = itemData.nextToken().trim();
			String itemPrice = itemData.nextToken().trim();
			String itemCalories = itemData.nextToken().trim();
			// String itemPrice = "$".concat(price).concat(".00");
			Object[] row = { itemName, itemPrice, itemCalories };

			_tableModel.addRow(row);
		}

		// Set up table view and controller
		final TableView tableView = new TableView(_tableModel);
		tableView.setDataTemplateFocus(BackgroundFactory
				.createLinearGradientBackground(Color.BLUE, Color.WHITE,
						Color.BLUE, Color.WHITE));
		TableController tableController = new TableController(_tableModel,
				tableView);
		tableController.setFocusPolicy(TableController.ROW_FOCUS);
		tableView.setController(tableController);
		tableView.setBackground(BackgroundFactory
				.createLinearGradientBackground(Color.GRAY, Color.WHITE,
						Color.GRAY, Color.WHITE));

		// Specify a simple data template for displaying 3 columns
		DataTemplate dataTemplate = new DataTemplate(tableView, NUM_ROWS,
				NUM_COLUMNS) {
			/**
			 * @see DataTemplate#getDataFields(int)
			 */
			public Field[] getDataFields(int modelRowIndex) {
				Object[] data = (Object[]) (_tableModel.getRow(modelRowIndex));
				Field[] fields = { new LabelField((String) data[0]),
						new LabelField((String) data[1]),
						new LabelField((String) data[2]) };
				return fields;
			}
		};

		dataTemplate.useFixedHeight(true);

		// Define regions and row height
		dataTemplate.setRowProperties(0, new TemplateRowProperties(ROW_HEIGHT));
		for (int i = 0; i < NUM_COLUMNS; i++) {
			dataTemplate.createRegion(new XYRect(i, 0, 1, 1));
			dataTemplate.setColumnProperties(i, new TemplateColumnProperties(
					Display.getWidth() / NUM_COLUMNS));
		}

		// Apply the template to the view
		tableView.setDataTemplate(dataTemplate);

		add(tableView);
		tableView.setFocusListener(getFocusListener());
		tableView.isFocusable();
		tableView.isSelectable();

		addMenuItem(new MenuItem("View Item", 10, 20) {
			public void run() {
				PaceMenuItem item = (PaceMenuItem) _items.elementAt(tableView
						.getRowNumberWithFocus());
				ItemScreen is = new ItemScreen(item, order);
				UiApplication.getUiApplication().pushScreen(is);
			}
		});

		addMenuItem(new MenuItem("View order", 10, 20) {
			public void run() {
				try {
					OrderScreen os = new OrderScreen(order);
					UiApplication.getUiApplication().pushScreen(os);
				} catch (Exception e) {
					Dialog.alert(e.toString());
				}
			}
		});
	}

	/**
	 * Adapter for displaying BlackBerryDevice objects in a table format
	 */
	private class MenuTableModelAdapter extends TableModelAdapter {
		/**
		 * @see net.rim.device.api.ui.component.table.TableModelAdapter#getNumberOfRows()
		 */
		public int getNumberOfRows() {
			return _items.size();
		}

		/**
		 * @see net.rim.device.api.ui.component.table.TableModelAdapter#getNumberOfColumns()
		 */
		public int getNumberOfColumns() {
			return NUM_COLUMNS;
		}

		/**
		 * @see net.rim.device.api.ui.component.table.TableModelAdapter#doAddRow(Object)
		 */
		protected boolean doAddRow(Object row) {
			Object[] arrayRow = (Object[]) row;
			_items.addElement(new PaceMenuItem((String) arrayRow[0],
					(String) arrayRow[1], (String) arrayRow[2]));
			return true;
		}

		/**
		 * @see net.rim.device.api.ui.component.table.TableModelAdapter#doGetRow(int)
		 */
		protected Object doGetRow(int index) {
			PaceMenuItem item = (PaceMenuItem) _items.elementAt(index);
			Object[] row = { item.getName(), item.getPrice(),
					item.getCalories() };
			return row;
		}
	}
}

// public void focusChanged(Field field, int eventType) {
// // TODO Auto-generated method stub
// // if(eventType == FocusChangeListener.FOCUS_GAINED)
// // {
// // rowIndex = field.getIndex();
// // }
// if (field.getVisualState() == Field.VISUAL_STATE_FOCUS)
// {
// rowIndex =
// }
// }
