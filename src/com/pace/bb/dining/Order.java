package com.pace.bb.dining;

import java.util.Vector;
import net.rim.device.api.system.*;

public class Order {

	private Vector _items;
	private int _total;
	
	public Order() {
		_total = 0;
		_items = new Vector();
	}

	public Order(Vector items) {
		_total = 0;
		_items = items;
	}

	public int addItemToOrder(PaceMenuItem item) {
		_items.addElement(item);
		int price = Integer.parseInt(item.getPrice());
		this.update_total(price);
		return get_total();
	}

	public void set_total(int total) {
		this._total = total;
	}

	public void update_total(int price) {
		_total = _total + price;
	}

	public int get_total() {
		return _total;
	}

	public Vector get_items() {
		return _items;
	}

}
