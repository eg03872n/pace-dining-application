package com.pace.bb.dining;

import java.io.EOFException;
import java.io.IOException;
import java.io.InputStream;

import net.rim.device.api.command.Command;
import net.rim.device.api.command.CommandHandler;
import net.rim.device.api.command.ReadOnlyCommandMetadata;
import net.rim.device.api.io.LineReader;
import net.rim.device.api.system.Bitmap;
import net.rim.device.api.system.Display;
import net.rim.device.api.ui.Color;
import net.rim.device.api.ui.Field;
import net.rim.device.api.ui.FieldChangeListener;
import net.rim.device.api.ui.Manager;
import net.rim.device.api.ui.MenuItem;
import net.rim.device.api.ui.UiApplication;
import net.rim.device.api.ui.component.BitmapField;
import net.rim.device.api.ui.component.ButtonField;
import net.rim.device.api.ui.component.Dialog;
import net.rim.device.api.ui.component.LabelField;
import net.rim.device.api.ui.container.HorizontalFieldManager;
import net.rim.device.api.ui.container.MainScreen;
import net.rim.device.api.ui.container.VerticalFieldManager;
import net.rim.device.api.ui.decor.BackgroundFactory;
import net.rim.device.api.util.StringProvider;

public class LoggedInPaceEatsScreen extends MainScreen implements
		FieldChangeListener {

	private VerticalFieldManager vManager;
	private HorizontalFieldManager hManager;
	private ButtonField buttonViewMenu;
	private BitmapField bitmapField;
	private ButtonField buttonOrder;

	public LoggedInPaceEatsScreen(String string) {
		super(MainScreen.VERTICAL_SCROLL | MainScreen.VERTICAL_SCROLLBAR);
		// TODO Auto-generated constructor stub
		LabelField title = new LabelField(
				"Welcome to Pace Eats, ".concat(string.toUpperCase()),
				LabelField.ELLIPSIS | LabelField.USE_ALL_WIDTH);

		hManager = new HorizontalFieldManager() {

		};

		hManager.add(title);
		this.add(hManager);

		hManager.setBackground(BackgroundFactory
				.createLinearGradientBackground(Color.WHITE, Color.BLUE,
						Color.WHITE, Color.BLUE));

		vManager = new VerticalFieldManager(Manager.VERTICAL_SCROLL
				| Manager.VERTICAL_SCROLLBAR) {

			protected void sublayout(int maxWidth, int maxHeight) {
				int width = Display.getWidth();
				int height = Display.getHeight() - hManager.getHeight();

				super.sublayout(width, height);
				setExtent(width, height);
			}
		};

		vManager.setBackground(BackgroundFactory
				.createLinearGradientBackground(Color.WHITE, Color.BLUE,
						Color.WHITE, Color.BLUE));

		Bitmap paceBitmap = Bitmap.getBitmapResource("img/background.png");
		bitmapField = new BitmapField(paceBitmap);

		vManager.add(bitmapField);

		buttonViewMenu = new ButtonField("View Today's Menu",
				ButtonField.CONSUME_CLICK);
		buttonViewMenu.setChangeListener(this);

		buttonOrder = new ButtonField("Place Order", ButtonField.CONSUME_CLICK);
		buttonOrder.setChangeListener(this);

		vManager.add(buttonViewMenu);
		vManager.add(buttonOrder);

		addMenuItem(new TableAdapterScreenMenuItem());
		this.add(vManager);

		addMenuItem(new MenuItem("Logout", 10, 20) {
			public void run() {
				// UiApplication.getUiApplication().pushScreen(screen);
			}
		});

		addMenuItem(new MenuItem("Start Order", 10, 20) {
			public void run() {
				Order order = new Order();
				TableAdapterScreen ts = new TableAdapterScreen(
						getStringTokenizer(), order);
				UiApplication.getUiApplication().pushScreen(ts);
			}
		});

		addMenuItem(new MenuItem("About", 10, 20) {
			public void run() {
				AboutPopupScreen as = new AboutPopupScreen();

				UiApplication.getUiApplication().pushScreen(as);
			}
		});
	}

	public void fieldChanged(Field field, int context) {
		if (field == buttonViewMenu) {
			Order order = new Order();
			TableAdapterScreen ts = new TableAdapterScreen(
					getStringTokenizer(), order);
			UiApplication.getUiApplication().pushScreen(ts);
		}
	}

	public DemoStringTokenizer getStringTokenizer() {

		InputStream stream = getClass().getResourceAsStream(
				"/resources/menu.txt");

		LineReader lineReader = new LineReader(stream);
		final StringBuffer buffer = new StringBuffer();

		while (true) {
			try {
				buffer.append(new String(lineReader.readLine()));
				buffer.append("\n");
				System.out.println(buffer);

			} catch (EOFException eof) {
				// We've reached the end of the file
				break;
			} catch (final IOException ioe) {
				UiApplication.getUiApplication().invokeLater(new Runnable() {
					public void run() {
						Dialog.alert("LineReader#readLine() threw "
								+ ioe.toString());
					}
				});
			}
		}

		String data = buffer.toString();

		data = data.replace('\r', ',');
		data = data.replace('\n', ',');

		return new DemoStringTokenizer(data);
	}

	private class TableAdapterScreenMenuItem extends MenuItem {
		/**
		 * Create a TableAdapterScreenMenuItem object
		 */
		public TableAdapterScreenMenuItem() {

			super(new StringProvider("View Today's Menu"), 0x230010, 0);

			this.setCommand(new Command(new CommandHandler() {
				/**
				 * @see net.rim.device.api.command.CommandHandler#execute(ReadOnlyCommandMetadata,
				 *      Object)
				 */
				public void execute(ReadOnlyCommandMetadata metadata,
						Object context) {
					Order order = new Order();
					TableAdapterScreen ts = new TableAdapterScreen(
							getStringTokenizer(), order);
					UiApplication.getUiApplication().pushScreen(ts);
				}
			}));
		}
	}
}
