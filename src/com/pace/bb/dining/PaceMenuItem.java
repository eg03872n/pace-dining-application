package com.pace.bb.dining;

/**
 * A class encapsulating name, model number and <code>Bitmap</code> image for a
 * BlackBerry Device.
 */
public class PaceMenuItem {
	private String _name;
	private String _calories;
	private String _price;
	private String _image;

	private double price;
	private int calories;

	/**
	 * Creates a new menu item.
	 * 
	 * @param name
	 *            The name of the item
	 * @param calories
	 *            The calories in the item
	 * @param price
	 *            The cost of the item
	 */
	PaceMenuItem(String name, String price, String calories) {
		_name = name;
		_calories = calories;
		_price = price;
		this.price = Double.parseDouble(price);
		this.calories = Integer.parseInt(calories);
	}

	/**
	 * Retrieves the item name.
	 * 
	 * @return The name of the menu item.
	 */
	public String getName() {
		return _name;
	}

	/**
	 * Retrieves the total calories.
	 * 
	 * @return The number of calories in the item.
	 */
	public String getCalories() {
		return _calories;
	}

	/**
	 * Retrieves the item price.
	 * 
	 * @return The price of the item
	 */
	public String getPrice() {
		return _price;
	}

	public double getDoublePrice() {
		return price;
	}
					
	public void set_image(String _image) {
		this._image = _image;
	}

	public String get_image() {
		return _image;
	}

}