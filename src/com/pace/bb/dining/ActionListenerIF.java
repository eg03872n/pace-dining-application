package com.pace.bb.dining;

/** actionListener interface */
public interface ActionListenerIF {

	void actionPerformed(Object source);

}//end interface ActionListenerIF