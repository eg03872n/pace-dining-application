package com.pace.bb.dining;

import java.util.Vector;

import net.rim.device.api.ui.MenuItem;
import net.rim.device.api.ui.UiApplication;
import net.rim.device.api.ui.component.LabelField;
import net.rim.device.api.ui.container.MainScreen;
import net.rim.device.api.ui.container.VerticalFieldManager;

public class OrderScreen extends MainScreen {

	public Vector _items;
	public Vector labelField;
	public Order _order;

	/**
	 * 
	 */
	public OrderScreen(Order order) {
		super(MainScreen.VERTICAL_SCROLL | MainScreen.VERTICAL_SCROLLBAR);
		// TODO Auto-generated constructor stub
		_order = order;
		_items = _order.get_items();
		VerticalFieldManager vfm = new VerticalFieldManager();
		LabelField[] lbl = new LabelField[_items.size()];
		for (int i = 0; i < _items.size(); i++) {
			lbl[i] = new LabelField(_items.elementAt(i));
		}

		int size = lbl.length;

		for (int x = 0; x < size; x++) {
			vfm.add(lbl[x]);
		}
		LabelField lblTotal = new LabelField(
				Integer.toString(order.get_total()));
		vfm.add(lblTotal);
		add(vfm);
		addMenuItem(new MenuItem("Place order", 10, 20) {
			public void run() {
				AboutPopupScreen as = new AboutPopupScreen();
				UiApplication.getUiApplication().pushScreen(as);
			}
		});
		addMenuItem(new MenuItem("About", 10, 20) {
			public void run() {
				AboutPopupScreen as = new AboutPopupScreen();
				UiApplication.getUiApplication().pushScreen(as);
			}
		});
	}

}
