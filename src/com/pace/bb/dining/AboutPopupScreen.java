package com.pace.bb.dining;

import net.rim.device.api.ui.Field;
import net.rim.device.api.ui.FieldChangeListener;
import net.rim.device.api.ui.component.ButtonField;
import net.rim.device.api.ui.component.LabelField;
import net.rim.device.api.ui.component.SeparatorField;
import net.rim.device.api.ui.container.PopupScreen;
import net.rim.device.api.ui.container.VerticalFieldManager;

public class AboutPopupScreen extends PopupScreen implements FieldChangeListener {

	private LabelField firstname, secondname, emptyfield, univname, coursename, semester;
	private ButtonField btnfldCancel;
	private SeparatorField sep;
	
	/**
	 * 
	 */
	public AboutPopupScreen() {
		super(new VerticalFieldManager());
		LabelField labelField = new LabelField("About",
				Field.FIELD_HCENTER);
		add(labelField);
		
		sep = new SeparatorField();
		this.add(sep);

		firstname = new LabelField("Eric Greene");
		this.add(firstname);

		secondname = new LabelField("George Vaitsos");
		this.add(secondname);

		emptyfield = new LabelField("");
		this.add(emptyfield);

		univname = new LabelField("Pace University");
		this.add(univname);

		coursename = new LabelField("CS-631");
		this.add(coursename);
		
		semester = new LabelField("Fall 2011");
		this.add(semester);

		emptyfield = new LabelField("");
		this.add(emptyfield);


        
		btnfldCancel = new ButtonField("Cancel", ButtonField.CONSUME_CLICK);
		btnfldCancel.setChangeListener(this);
		this.add(btnfldCancel);
		
		
		// TODO Auto-generated constructor stub
	}		
	

	public void fieldChanged(Field field, int context) {
		if (field == btnfldCancel) {
			this.close();
		}
	}
}
