package com.pace.bb.dining;

import net.rim.device.api.ui.Field;
import net.rim.device.api.ui.FieldChangeListener;
import net.rim.device.api.ui.UiApplication;
import net.rim.device.api.ui.component.ButtonField;
import net.rim.device.api.ui.component.CheckboxField;
import net.rim.device.api.ui.component.EditField;
import net.rim.device.api.ui.component.LabelField;
import net.rim.device.api.ui.component.PasswordEditField;
import net.rim.device.api.ui.component.SeparatorField;
import net.rim.device.api.ui.container.PopupScreen;
import net.rim.device.api.ui.container.VerticalFieldManager;

public class LoginPopupScreen extends PopupScreen implements FieldChangeListener {

	private EditField username;
	private PasswordEditField password;
	private CheckboxField remember;
	private ButtonField btnfldCancel;
	private ButtonField btnfldLogin;
	private SeparatorField sep;

	/**
	 * 
	 */
	public LoginPopupScreen() {
		super(new VerticalFieldManager());
		LabelField labelField = new LabelField("Pace Eats",
				Field.FIELD_HCENTER);
		add(labelField);
	
		username = new EditField("Username:", "");
		this.add(username);
		
		sep = new SeparatorField();
		this.add(sep);
		
		password = new PasswordEditField("Password:", "");
        this.add(password);
        
        remember = new CheckboxField("Remember me", false);
        this.add(remember);
        
        btnfldLogin = new ButtonField("Sign in", ButtonField.CONSUME_CLICK);
		btnfldCancel = new ButtonField("Cancel", ButtonField.CONSUME_CLICK);
		btnfldCancel.setChangeListener(this);
		btnfldLogin.setChangeListener(this);
		this.add(btnfldLogin);
		this.add(btnfldCancel);
		
		// TODO Auto-generated constructor stub
	}
	
	public void fieldChanged(Field field, int context) {
		if (field == btnfldCancel) {
			this.close();
		} else if (field == btnfldLogin) {
			// Add authentication code - or call Login method from another class?
			LoggedInPaceEatsScreen ps = new LoggedInPaceEatsScreen(this.username.toString());
			UiApplication.getUiApplication().pushScreen(ps);
		}
	}

}
